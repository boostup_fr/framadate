FROM php:8.1.1-fpm-bullseye as base
ENV BUILD_DEPS \
        git \
        unzip \
        build-essential \
        libssl-dev \
        libonig-dev \
        zlib1g-dev \
        libicu-dev \
        g++ \
        libxml2-dev \
        ca-certificates

RUN apt-get update \
    && apt-get install -y --no-install-recommends $BUILD_DEPS \
    && update-ca-certificates \
    && docker-php-ext-install mbstring intl xml pdo pdo_mysql opcache \
    && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*


FROM base as base-optimized
# set recommended PHP.ini settings
# see https://secure.php.net/manual/en/opcache.installation.php
RUN { \
		echo 'opcache.memory_consumption=128'; \
		echo 'opcache.interned_strings_buffer=8'; \
		echo 'opcache.max_accelerated_files=4000'; \
		echo 'opcache.revalidate_freq=60'; \
		echo 'opcache.fast_shutdown=1'; \
		echo 'opcache.enable_cli=1'; \
} > /usr/local/etc/php/conf.d/opcache-recommended.ini
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
RUN { \
		echo 'date.timezone = UTC'; \
} > /usr/local/etc/php/conf.d/framadate.ini


FROM base as builder

RUN apt-get update \
    && apt-get install -y --no-install-recommends curl wget gnupg dirmngr xz-utils libatomic1 \
    && update-ca-certificates \
    && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN curl -sS https://raw.githubusercontent.com/composer/getcomposer.org/faf828b1635a601ba364a7ee599250b2690a91ab/web/installer -o composer-setup.php \
 && php composer-setup.php --version="2.2.3" --install-dir=/usr/local/bin --filename=composer \
 && composer --version \
 && rm composer-setup.php

WORKDIR /var/www/app
RUN chown -R www-data:www-data /var/www
USER www-data


FROM builder as builder-with-vendors
ADD composer.json composer.lock /var/www/app/
RUN composer --version \
 && composer install  \
    --ignore-platform-reqs \
    --no-ansi \
    --no-autoloader \
    --no-interaction \
    --no-scripts


FROM builder-with-vendors as build
ADD --chown=www-data:www-data . /var/www/app
RUN composer install --optimize-autoloader


FROM base-optimized as runner
ENV LOG errorlog
CMD ["php-fpm"]
WORKDIR /var/www/app
RUN mkdir tpl_c && chown www-data: -R tpl_c

COPY --from=build \
    /var/www/app/ \
    /var/www/app/


FROM nginx:1.21.5 as server
ADD ./nginx.conf /etc/nginx/conf.d/default.conf
ADD . /var/www/app
RUN rm -R /var/www/app/composer.* /var/www/app/nginx.conf /var/www/app/tpl/ /var/www/app/app/
